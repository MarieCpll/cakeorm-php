<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Psr\\SimpleCache\\' => array($vendorDir . '/psr/simple-cache/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'Cake\\Validation\\' => array($vendorDir . '/cakephp/validation'),
    'Cake\\Utility\\' => array($vendorDir . '/cakephp/utility'),
    'Cake\\ORM\\' => array($vendorDir . '/cakephp/orm'),
    'Cake\\Log\\' => array($vendorDir . '/cakephp/log'),
    'Cake\\Event\\' => array($vendorDir . '/cakephp/event'),
    'Cake\\Datasource\\' => array($vendorDir . '/cakephp/datasource'),
    'Cake\\Database\\' => array($vendorDir . '/cakephp/database'),
    'Cake\\Core\\' => array($vendorDir . '/cakephp/core'),
    'Cake\\Collection\\' => array($vendorDir . '/cakephp/collection'),
    'Cake\\Cache\\' => array($vendorDir . '/cakephp/cache'),
);
