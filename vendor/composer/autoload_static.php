<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit5b20c034636c0f014727196ed7b528e3
{
    public static $files = array (
        '72142d7b40a3a0b14e91825290b5ad82' => __DIR__ . '/..' . '/cakephp/core/functions.php',
        '948ad5488880985ff1c06721a4e447fe' => __DIR__ . '/..' . '/cakephp/utility/bootstrap.php',
        '028fdea3165c4ba1ecccc83b7fec69fc' => __DIR__ . '/..' . '/cakephp/collection/functions.php',
    );

    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'Psr\\SimpleCache\\' => 16,
            'Psr\\Log\\' => 8,
            'Psr\\Http\\Message\\' => 17,
        ),
        'C' => 
        array (
            'Cake\\Validation\\' => 16,
            'Cake\\Utility\\' => 13,
            'Cake\\ORM\\' => 9,
            'Cake\\Log\\' => 9,
            'Cake\\Event\\' => 11,
            'Cake\\Datasource\\' => 16,
            'Cake\\Database\\' => 14,
            'Cake\\Core\\' => 10,
            'Cake\\Collection\\' => 16,
            'Cake\\Cache\\' => 11,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Psr\\SimpleCache\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/simple-cache/src',
        ),
        'Psr\\Log\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/log/Psr/Log',
        ),
        'Psr\\Http\\Message\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/http-message/src',
        ),
        'Cake\\Validation\\' => 
        array (
            0 => __DIR__ . '/..' . '/cakephp/validation',
        ),
        'Cake\\Utility\\' => 
        array (
            0 => __DIR__ . '/..' . '/cakephp/utility',
        ),
        'Cake\\ORM\\' => 
        array (
            0 => __DIR__ . '/..' . '/cakephp/orm',
        ),
        'Cake\\Log\\' => 
        array (
            0 => __DIR__ . '/..' . '/cakephp/log',
        ),
        'Cake\\Event\\' => 
        array (
            0 => __DIR__ . '/..' . '/cakephp/event',
        ),
        'Cake\\Datasource\\' => 
        array (
            0 => __DIR__ . '/..' . '/cakephp/datasource',
        ),
        'Cake\\Database\\' => 
        array (
            0 => __DIR__ . '/..' . '/cakephp/database',
        ),
        'Cake\\Core\\' => 
        array (
            0 => __DIR__ . '/..' . '/cakephp/core',
        ),
        'Cake\\Collection\\' => 
        array (
            0 => __DIR__ . '/..' . '/cakephp/collection',
        ),
        'Cake\\Cache\\' => 
        array (
            0 => __DIR__ . '/..' . '/cakephp/cache',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit5b20c034636c0f014727196ed7b528e3::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit5b20c034636c0f014727196ed7b528e3::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
