<?php

require 'vendor/autoload.php';

//connection vers BDD
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;

ConnectionManager::config('default', [
	'className' => 'Cake\Database\connection',
	'driver' => 'Cake\Database\Driver\Mysql',
	'database' => 'cakeorm',
	'username' => 'root',
	'password' => 'root',
	'host' => 'localhost'

]);


//pour aller chercher ma table posts dans BDD
$posts = TableRegistry::get('Posts');

//affiche le nom du premier post
foreach ($posts->find() as $post) {
	echo $post->name . '>br>';
}

?>